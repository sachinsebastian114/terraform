variable "project_id" {
  description = "Your GCP project ID"
}

variable "region" {
  description = "GCP region for resources"
}

variable "zone" {
  description = "GCP zone for resources"
}