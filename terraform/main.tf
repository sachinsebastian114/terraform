provider "google" {
  credentials = <<EOF
  {
  "type": "service_account",
  "project_id": "terraform-project-419314",
  "private_key_id": "df923d04dda4d1bed5db1ca57c49b4e80846106a",
  "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDtdmTlZQ6Ukzhc\nzFky6J1Zb/Vtwu1RSNcerw2h/APzQET7qaRpLjL+3EGzWRa3d3tL0yKA/F4Vm7lR\nszEFSk6W4vCO75OEZhMPJIfUJQ7jmXyZO7g8kb9YYkT+zuH47+Rh2MQzCzhOcGK3\ntYJWuX4wJIpoxeTASWaUpc/NZ0tEjJjIMJvBe+BvMjdPo8KOMmAzK6KLBNboNYjA\n4g+DytnL8KE0DXEeYlcE/5Z41Udo36FZUsJ5erCeu2R7INcBQ/elcGzvA/aaQE6+\nTWY11vOL/yMQMBM/LpICzchJX7mL6g9bkUK4P2LSX3A09ovX9tvAoUrlCOVjSdg0\nGaSpzJg9AgMBAAECggEAXn26t3eHnkKY7oBYTEQ4/NJq0LL82VgJVgnlfFw6vCyK\nZrRCGqKKI1VFBZeBTR9L3kfVVotV6vZM/4A1FPxVGoCSpeYahthRZDiunhHjA1lb\nteQIM33xrLtp5hUv4W2gLPljVUZ1MO1SsvSUuWrVllWsB/YbSRLMMYFrl7ovPsaJ\nZYnlxwzLGWt4e4kkxrMUlfnexeq+UraJh/3txV4EbPokgtMrMRj6PzemyMhwHJtv\nd+OnBJEWbiQzy4h4hYqwWCLBImZ9jxsd7RHpK/XzR43koMUgN//nceLZNEkj9AYW\nvP4SfymstiU38M879w8zDeBEpdp1iHYKKH8pYsnjZwKBgQD9t14SRWugIgOs8/vQ\n01Rg7K/q6YxXb8Mqass1uzNftOCQo3gyUDtVBFqvv+vyyFRMwiEmhe/jOlzSSbME\nU4BgyirYyTdpZuVZNJrrVQKhlH7mMgIAjcIV+Nt4oFfWNnWdpi0YjWO3+1XziIzZ\nGOqmb47djsbbi9lXHmGje/OyLwKBgQDvmZLKV8VomeHopoe/eI7ahtI2jPG2/j4I\nxTUr3cmi7UXu1eaKZ5FwpH4bO0NFQQdzIGhpKddZ/eS9KZ4KiNyWkZQHlp535JX7\nCcefVUgQedLDb50zdFbb+kvOtY7m643K8d4mVi+9++TTjeWC4MkSbtZf1AavGndG\nxj258O2dUwKBgGxGFJ83gPaVjXwBozU6+odierCFep7V0zQG3zHO8d2mEy+mk2Tl\nWGyvrwPRo5+4XkFjl0IZZoZSIcqcKQlvmRm1LRPWyw7wGnA3QuWrIbRS6tTSAoi5\n9lnqGb8grlt8YjUk9ZnEvQmG+Oc1HuaG+5yLfyyOAc8vn1DOZlyeSSGBAoGBAJ2d\ngRrqskolMJ9DLXmfbRoEJrUL8tZWWaibUmcQ91veimqa+5WAnARlsS9oxNAB/0yU\nvRqxE6F3i8mEXxq+/Z1vFtoHEyf7j04dGNyP1WBBQ4ydF+SJW7pJ5SSN+vudaSFg\nCqtddvqbgeRJK/n0tVES6KMRb3QxDNXaAzEWdLlRAoGAB73Yw9pVPQRvnY/cItac\n38ZAbTYszWOd6rCeo8cz+HYvUoxhOCFaq/BX84zuLJceJh9fK5lA2EenNOna6dT8\ns2EAHcpHk38jzELL6MY0WtyoY62/N845nzWnb9lO2rXSmxjWnT/4WzZH/5D+2vDB\ndF0W35ryVWHs1t7fYpUz1pM=\n-----END PRIVATE KEY-----\n",
  "client_email": "terraform@terraform-project-419314.iam.gserviceaccount.com",
  "client_id": "103183083580767140571",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://oauth2.googleapis.com/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/terraform%40terraform-project-419314.iam.gserviceaccount.com",
  "universe_domain": "googleapis.com"
}
EOF
}
  project     = var.project_id
  region      = var.region
  
resource "google_compute_instance" "web_server" {
  name         = "web-server"
  machine_type = "n1-standard-1"
  zone         = var.zone
  tags         = ["http-server"]

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
    }
  }

  network_interface {
    network = "default"
    access_config {
      // Ephemeral IP
    }
  }
}

resource "google_sql_database_instance" "database_instance" {
  name             = "database-instance"
  database_version = "MYSQL_5_7"
  region           = var.region
  settings {
    tier = "db-f1-micro"
  }
}

